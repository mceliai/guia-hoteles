$(function(){
    $("[data-toggle='tooltip']").tooltip()
    $("[data-toggle='popover']").popover()
    $('.carousel').carousel({
      interval: 2000
    });

    $('#contactoSPablo').on('show.bs.modal', function (e){
      console.log('el modal contacto se está mostrando');

      $('#contactoBtn').removeClass('btn-outline-success');
      $('#contactoBtn').addClass('btn-primary');
      $('#contactoBtn').prop('disabled', true);

    });
    $('#contactoSPablo').on('shown.bs.modal', function (e){
      console.log('el modal contacto se mostró');
    });
    $('#contactoSPablo').on('hide.bs.modal', function (e){
      console.log('el modal contacto se oculta');
    });
    $('#contactoSPablo').on('hidden.bs.modal', function (e){
      console.log('el modal contacto se ocultó');

      $('#contactoBtn').removeClass('btn-primary');
      $('#contactoBtn').addClass('btn-outline-success');
      $('#contactoBtn').prop('disabled', false);            
    });
    

    $('#suscribete').on('show.bs.modal', function (e){
      console.log('el modal de suscripción se está mostrando');

      $('#suscribeteBtn').removeClass('btn-outline-success');
      $('#suscribeteBtn').addClass('btn-primary');
      $('#suscribeteBtn').prop('disabled', true);
    });

    $('#suscribete').on('hidden.bs.modal', function (e){
      console.log('el modal contacto se ocultó');

      $('#suscribeteBtn').removeClass('btn-primary');
      $('#suscribeteBtn').addClass('btn-outline-success');
      $('#suscribeteBtn').prop('disabled', false);            
    });
  });